## playwavmini
WAVE or RIFF WAVE sound file format player using Simple DirectMedia Layer (SDL) version 2.0.x

# How to use playwav player

Call `playwav` with argument .WAV file name to play.

# How to build

The following steps build build `playwav.exe` on a MSYS2/MinGW-w64 box, or `playwav` on a GNU/Linux box, using SDL2 library (sdl2-config) and make.

```
$ gcc `pkg-config --cflags sdl2` -O3 -Wall -c playwav_wSDL.c
$ gcc playwav_wSDL.o -o playwav_wSDL `pkg-config --libs sdl2`
```

where
> $ pkg-config --cflags sdl2

gives
> -Dmain=SDL_main -I/mingw/include

where
> $ pkg-config --libs sdl2

gives
> -mwindows -L/mingw/lib -lmingw32 -lSDL2main -lSDL2

The following steps build `playwav` on a Ubuntu/Debian/GNU/Linux box, using SDL2 library (pck-config) and CMake.

```
$ mkdir build
$ cd build
$ cmake -D CMAKE_BUILD_TYPE=Release ..
$ make playwav
```

The following steps build `playwav.exe` on a Windows o.s. box with MSVC, vcpkg, SDL2 installed with vcpkg, CMake.

You can have Your build environment set, on a Windows 10 box, if Your MSVC is Microsoft Visual Studio 2019 Community edition, using Windows 10 taskbar search box writing `x64 Native Tools Command Prompt for VS 2019` and starting matching App.
Otherwise, if MSVC is installed in default localtion, if Windows SDK is 10.0.18362.0 (please see what is in 'C:\Program Files (x86)\Microsoft SDKs\Windows Kits\10\ExtensionSDKs\Microsoft.UniversalCRT.Debug' folder) (see [2]) issuing

```
C:\>"C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat" amd64 10.0.18362.0
```

Then

```
C:\playwav>mkdir build
C:\playwav>cd build
C:\playwav\build>cmake -D CMAKE_TOOLCHAIN_FILE=C:/Users/gzaff/Devs/vcpkg/scripts/buildsystems/vcpkg.cmake ..
C:\playwav\build>msbuild playwav.sln -target:Rebuild -property:Configuration=Release
```

For the case that Visual Studio can be used

```
C:\playwav>mkdir build
C:\playwav>cd build
C:\playwav\build>cmake -G "Visual Studio 16 2019" -A x64 -T host=x64 -D CMAKE_TOOLCHAIN_FILE=C:/Users/gzaff/Devs/vcpkg/scripts/buildsystems/vcpkg.cmake ..
```

For building from command line

```
C:\playwav\build>cmake --build . --config Release --target playwav
```

Otherwise start Microsoft Visual Studio and debug playwavmini solution.

You can have Your build environment set, on a Windows 7 box, if Your MSVC is Microsoft Visual Studio 2017 Community edition, using Windows 7 taskbar search box writing `x64 Native Tools Command Prompt for VS 2017` and starting matching App.
Otherwise, if MSVC is installed in default localtion, if Windows SDK is 10.0.17763.0 (please see what is in 'C:\Program Files (x86)\Microsoft SDKs\Windows Kits\10\ExtensionSDKs\Microsoft.UniversalCRT.Debug' folder) (see [2]) issuing

```
C:\>"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat" amd64 10.0.17763.0
```

Then

```
C:\playwav>mkdir build
C:\playwav>cd build
C:\playwav\build>cmake -D CMAKE_TOOLCHAIN_FILE=C:/Users/gzaff/Devs/vcpkg/scripts/buildsystems/vcpkg.cmake ..
C:\mdxmini\build>msbuild playwav.sln -target:Rebuild -property:Configuration=Release
```

For the case that Visual Studio can be used

```
C:\playwav>mkdir build
C:\playwav>cd build
C:\playwav\build>cmake -G "Visual Studio 15 2017 Win64" -T host=x64 -D CMAKE_TOOLCHAIN_FILE=C:/Users/gzaff/Devs/vcpkg/scripts/buildsystems/vcpkg.cmake ..
```

For building from command line

```
C:\mdxmini\build>cmake --build . --config Release --target playwav
```

Otherwise start Microsoft Visual Studio and debug playwavmini solution.

[1]
it is make-utility name e.g. `mingw32-make` with specified PATH if make is not within search PATH as it should be

[2]
calling vcvarsall.bat update PATH variable, so "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\IDE\CommonExtensions\Microsoft\CMake\CMake\bin\cmake" and "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\IDE\CommonExtensions\Microsoft\CMake\Ninja\ninja" can be called as cmake and ninja respectively

# Links
Reference information pages about how to install and how to use Vcpkg

[GitHub Microsoft vcpkg](https://github.com/Microsoft/vcpkg)

[vcpkg: A C++ package manager for Windows, Linux and MacOS](https://docs.microsoft.com/en-us/cpp/build/vcpkg?view=vs-2019)

[Eric Mittelette's blog](https://devblogs.microsoft.com/cppblog/vcpkg-a-tool-to-acquire-and-build-c-open-source-libraries-on-windows/)

[Audio File Format Specifications](http://www-mmsp.ece.mcgill.ca/Documents/AudioFormats/WAVE/WAVE.html) 
