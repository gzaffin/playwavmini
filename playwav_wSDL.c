﻿/*
 * build instruction
 * minGW
 * $ x86_64-w64-mingw32-gcc-4.8.2 -Wall -O3 -c playwav_wSDL.c -I/mingw/include -Dmain=SDL_main
 * $ x86_64-w64-mingw32-gcc-4.8.2 playwav_wSDL.o -o playwav_wSDL.exe -L/mingw/lib -lmingw32 -lSDL2main -lSDL2 -mwindows
 * otherwise
 * $ x86_64-w64-mingw32-gcc-4.8.2 `pkg-config --cflags sdl2` -O3 -Wall -c playwav_wSDL.c
 * when
 * $ pkg-config --cflags sdl2
 * gives
 * -Dmain=SDL_main -I/mingw/include
 * $ x86_64-w64-mingw32-gcc-4.8.2 playwav_wSDL.o -o playwav_wSDL.exe `pkg-config --libs sdl2`
 * when
 * $ pkg-config --libs sdl2
 * gives
 * -mwindows -L/mingw/lib -lmingw32 -lSDL2main -lSDL2
 * Windows Microsoft Visual Studio, vcpkg, CMake
 * mkdir build
 * cd build
 * cmake -D CMAKE_TOOLCHAIN_FILE=C:/Users/gzaff/Devs/vcpkg/scripts/buildsystems/vcpkg.cmake ..
 * msbuild playwav.sln -target:Rebuild -property:Configuration=Release
 */

#include <stdio.h>
#include <string.h>
#include <SDL.h>

// prototype for our audio callback
// see the implementation for more information
void my_audio_callback(void *userdata, Uint8 *stream, int len);

typedef struct {
	FILE *fp;
	unsigned long pcmsize;
	unsigned int freq;
	unsigned int bit;
	unsigned int ch;
} wavfmt_t;

unsigned long dword_le(unsigned char *ptr);
unsigned short word_le(unsigned char *ptr);
int wavGetFormat(wavfmt_t *lf);
int wavSeekDataChunk(wavfmt_t *lf);
long wavConvBit(unsigned data, int inBit, int outBit);
int wavGetSample(wavfmt_t *lf, int outBit);
void wavClose(wavfmt_t *wfmt);
int wavOpen(char *file, wavfmt_t *wfmt);

// variable declarations
static Uint8 *audio_pos = NULL; // global pointer to the audio buffer to be played
static Uint32 audio_len = 0u; // remaining length of the sample we have to play

unsigned long dword_le(unsigned char *ptr)
{
	return ((unsigned long)ptr[0]) | \
	(((unsigned long)ptr[1]) << 8) | \
	(((unsigned long)ptr[2]) << 16) | \
	(((unsigned long)ptr[3]) << 24);
}

unsigned short word_le(unsigned char *ptr)
{
	return ((unsigned long)ptr[0]) | \
	(((unsigned long)ptr[1]) << 8);
}

int wavGetFormat(wavfmt_t *lf)
{
	FILE *fp;
	unsigned char buf[10];

	fp = lf->fp;

	if (!fp) return -1;

	fread(buf,4,1,fp);
	if (memcmp(buf,"RIFF",4) !=0) {
		return -1; // not RIFF
	}

	fread(buf,4,1,fp); // size of RIFF

	fread(buf,4,1,fp);
	buf[4] = 0;
	printf("%s\n", buf);
	if (memcmp(buf,"WAVE",4) !=0) {
		return -1; // not WAVE
	}
	fread(buf,4,1,fp);
	buf[4] = 0;
	printf("%s\n", buf);
	if ((buf[0] != 'f') || (buf[1] != 'm') || (buf[2] != 't') || (buf[3] != ' ')) {
		return -1; // not "fmt "
	}

	fread(buf,4,1,fp); // chunk size

	fread(buf,2,1,fp); // type of format

	if (word_le(buf) != 1u) {
		printf("Error : WAVE file type wFormatTag = %u\n", word_le(buf));
		return -1; // not PCM
	}

	fread(buf,2,1,fp); // Channel
	lf->ch = word_le(buf);

	fread(buf,4,1,fp); // Frequency
	lf->freq = dword_le(buf);

	fread(buf,4,1,fp); // byte_per_sec
	fread(buf,2,1,fp); // sample size

	fread(buf,2,1,fp); // bit size
	lf->bit = word_le(buf);

	return 0;
}

int wavSeekDataChunk(wavfmt_t *lf)
{
	unsigned char buf[10];
	long lslen;

	FILE *fp;

	fp = lf->fp;

	do {
		fread(buf,4,1,fp);
		if (memcmp(buf,"data",4) == 0) {
			fread(buf,4,1,fp);
			lf->pcmsize = dword_le(buf);
			return 0;
		} else {
			fread(buf,4,1,fp);
			lslen = dword_le(buf);
			fseek(fp,lslen,SEEK_CUR);
		}
	} while (!feof(fp));
	return -1;
}

long wavConvBit(unsigned data, int inBit, int outBit)
{
	if (inBit==outBit)
	{
		return data;
	}

	if (inBit < outBit)
	{
		data <<= ( outBit - inBit );
	}
	else
	{
		data >>= ( inBit - outBit);
	}
	return data;
}

int wavGetSample(wavfmt_t *lf, int outBit)
{
	unsigned char buf[8];
	long mixch;

	mixch = 0;
	if (lf->bit == 8)
	{
		memset(buf,0x80,sizeof(buf)); // 8bit is unsigned
	}
	else
	{
		memset(buf,0,sizeof(buf));
	}

	if (lf->ch == 2)
	{
		// stereo
		if (lf->bit == 16)
		{
			// 16bit
			fread(buf,4,1,lf->fp);
			mixch = (short)word_le(buf);
			mixch += (short)word_le(buf+2);
			mixch /= 2;
		}
		else
		{
			// probably 8bit
			fread(buf,2,1,lf->fp);
			mixch = buf[0] - 0x80;
			mixch += ((long)buf[1] - 0x80);
			mixch /= 2;
		}
	}
	else
	{
		// mono
		if (lf->bit == 16)
		{
			// 16bit
			fread(buf,2,1,lf->fp);
			mixch = (short)word_le(buf);
		}
		else
		{
			// probably 8bit
			fread(buf,1,1,lf->fp);
			mixch = buf[0] - 0x80;
		}
	}

	mixch = wavConvBit(mixch, lf->bit, outBit);
	return mixch;
}

void wavClose(wavfmt_t *wfmt)
{
	fclose(wfmt->fp);
	wfmt->fp = NULL;
}

int wavOpen(char *file, wavfmt_t *wfmt)
{
	memset(wfmt,0,sizeof(wavfmt_t));

	wfmt->fp = fopen(file,"rb");
	if (!wfmt->fp)
	{
		printf("Error : File open error(%s)\n",file);
		return -1;
	}

	if (wavGetFormat(wfmt))
	{
		printf("Error : Unsupported format file\n");
		return -1;
	}

	printf("freq = %d,bit = %d,ch = %d\n", wfmt->freq,wfmt->bit,wfmt->ch);

	if (wavSeekDataChunk(wfmt))
	{
		printf("Error : data chank not found\n");
		wavClose(wfmt);
		return -1;
	}

	return 0;
}

/*
** PLAYING A SOUND IS MUCH MORE COMPLICATED THAN IT SHOULD BE
*/
int main(int argc, char* argv[]) {
	// local variables
	Uint32 wav_length; // length of our sample
	Uint8 *wav_buffer; // buffer containing our audio file
	SDL_AudioSpec wav_spec; // the specs of our piece of music
	SDL_AudioSpec obt_spec; // obtained specs

	wavfmt_t wav;

	printf("Stared! Ar U Xited?\n");
	fflush(stdout);

	SDL_version compiled;
	SDL_VERSION(&compiled);
	printf("compiled with SDL version %d.%d.%d\n", compiled.major, compiled.minor, compiled.patch);
	fflush(stdout);

	SDL_version linked;
	SDL_GetVersion(&linked);
	printf("linked SDL version %d.%d.%d\n", linked.major, linked.minor, linked.patch);
	fflush(stdout);

	// Initialize SDL.
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		fprintf(stderr, "Couldn't SDL_Init(%d)\n", SDL_INIT_AUDIO);
		fflush(stderr);
		return 1;
	}

	/* Log the list of available audio drivers */
	SDL_Log("Available audio drivers:");
	{
		int c;
		for (c = 0; c < SDL_GetNumAudioDrivers(); ++c) {
			SDL_Log("%i: %s", c, SDL_GetAudioDriver(c));
		}
	}

	/* Log used audio driver */
	SDL_Log("Using audio driver: %s\n", SDL_GetCurrentAudioDriver());

	if (wavOpen(argv[1], &wav) < 0) {
		return 1;
	} else {
		int i;
		wav_spec.freq = wav.freq;
		wav_spec.channels = wav.ch;
		if (wav.bit == 8) {
			wav_spec.format = AUDIO_S16;
		}
		if (wav.bit == 16) {
			wav_spec.format = AUDIO_S16;
		}
		if (wav.bit == 32) {
			wav_spec.format = AUDIO_S32;
		}

		for(i=0; i < 32; i++) {
			printf(" %4d ", wavGetSample(&wav, 16));
			if ((i + 1) % 8 == 0) {
				printf("\n");
			}
		}
		fflush(stdout);
		wavClose(&wav);
	}

	/* Load the WAV */
	// the specs, length and buffer of our wav are filled
	if( SDL_LoadWAV(argv[1], &wav_spec, &wav_buffer, &wav_length) == NULL ) {
		fprintf(stderr, "Couldn't open input file: %s\n", argv[1]);
		fprintf(stderr, "Returned string error: %s\n", SDL_GetError());
		fflush(stderr);
		return 1;
	} else {
		if (SDL_AUDIO_ISFLOAT(wav_spec.format)) {
			SDL_Log("floating point data\n");
		} else {
			SDL_Log("integer data\n");
		}
		SDL_Log("%d bits per sample\n", (int) SDL_AUDIO_BITSIZE(wav_spec.format));
		SDL_Log("%d samples\n", (int) SDL_AUDIO_BITSIZE(wav_spec.format));

		fprintf(stdout, "DSP frequency (samples per second) f = %d\n", wav_spec.freq);
		fprintf(stdout, "audio data format 0x%x\n", wav_spec.format);
		fprintf(stdout, "wav file length in bytes length = %u\n", wav_length);
		fflush(stdout);
	}
	// set the callback function
	wav_spec.callback = my_audio_callback;
	wav_spec.userdata = NULL;
	// set our global static variables
	audio_pos = wav_buffer; // copy sound buffer
	audio_len = wav_length; // copy file length in bytes

	/* Open the audio device */
	SDL_AudioDeviceID audio_id = SDL_OpenAudioDevice(NULL, 0, &wav_spec, &obt_spec, 0);
	if ( audio_id < 0 ) {
		fprintf(stderr, "Couldn't open audio: %s\n", SDL_GetError());
		fflush(stderr);
		exit(-1);
	}

	/* Start playing */
	int pause_on = 0;
	SDL_PauseAudioDevice(audio_id, pause_on);

	SDL_Event event;
	// wait until we're don't playing
	while ( audio_len > 0u ) {
		SDL_Delay(100);
		while ( SDL_TRUE == SDL_PollEvent(&event) ) {
			/*SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION, "[SDL] Event type: %x\n", event.type);*/
			switch ( event.type ) {
				case SDL_QUIT:
					audio_len = 0u;
					break;
				default:
					break;
			}
		}
	}

	// shut everything down
	SDL_CloseAudio();
	SDL_FreeWAV(wav_buffer);

	return 0;
}

// audio callback function
// here you have to copy the data of your audio buffer into the
// requesting audio buffer (stream)
// you should only copy as much as the requested length (len)
void my_audio_callback(void *userdata, Uint8 *stream, int len) {

	if ( audio_len != 0u ) {
		len = ( len > (int)audio_len ? (int)audio_len : len );
		SDL_memcpy (stream, audio_pos, len); // simply copy from one buffer into the other
		//SDL_MixAudio(stream, audio_pos, len, SDL_MIX_MAXVOLUME); // mix from one buffer into another

		audio_pos += len;
		audio_len -= (Uint32)len;
	}
}

